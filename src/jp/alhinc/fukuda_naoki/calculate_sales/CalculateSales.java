package jp.alhinc.fukuda_naoki.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CalculateSales {
	public static void main(String[] args) {
		HashMap<String, String> mapBranchList = new HashMap<String, String>();
		HashMap<String, Long> mapProceedsList = new HashMap<String, Long>();

		// コマンドパラメータ確認
		if ( args.length != 1 ) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		// lstファイル読み込み
		if ( !readLstFile(args[0], "branch.lst", mapBranchList, mapProceedsList) ) {
			return;
		}
		// rcdファイル読み込み
		if ( !readRcdFile(args[0], mapBranchList, mapProceedsList) ) {
			return;
		}
		// ファイル出力
		if ( !writeOutFile(args[0], "branch.out", mapBranchList, mapProceedsList) ) {
			return;
		}
	}

	private static boolean readLstFile (String filePath, String fileName, HashMap<String, String> branchList, HashMap<String, Long> proceedsList) {
		// Lstファイルパス設定
		FileReader frBranchList = null;
		File fileLst = new File(filePath, fileName);

		// ファイル存在確認
		if ( !fileLst.exists() ) {
			System.out.println("支店定義ファイルが存在しません");
			return false;
		}
		// Lstファイルの読み込み
		BufferedReader brBranchList = null;
		try {
			frBranchList = new FileReader(fileLst);
			brBranchList = new BufferedReader(frBranchList);
			String strLineBranchList = null;
			// 1行ずつ読み込む
			while ( ( strLineBranchList = brBranchList.readLine()) != null) {
				String[] strSplitedLine = strLineBranchList.split(",");
				// 支店コードのフォーマット確認と要素数が2つであるか確認
				String codePattern = "^\\d{3}$";
				if ( !strSplitedLine[0].matches(codePattern) ||
						strSplitedLine.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				// マップに情報格納
				branchList.put(strSplitedLine[0],strSplitedLine[1]);
				// 同時に売上マップも作成・初期化
				proceedsList.put(strSplitedLine[0], (long) 0);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try {
				if ( brBranchList != null ) {
					brBranchList.close();
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}

	private static boolean readRcdFile(String filePath, HashMap<String, String> branchList, HashMap<String, Long> proceedsList) {
		// 拡張子がrcd、且つファイル名が数字8桁のファイルを検索
		String salesFilePattern = "^\\d{8}.rcd$";
		File fileDir = new File(filePath);
		File[] dirFiles = fileDir.listFiles();
		int iMismatchFiles = 0;

		// ディレクトリから売上ファイルのみを抽出、リストを作成
		ArrayList<File> arrayProceedsList = new ArrayList<File>();
		for (int i = 0; i < dirFiles.length; i++) {
			// ファイル確認
			if ( !dirFiles[i].isFile() ) {
				iMismatchFiles++;
				continue;
			}
			// 名前確認
			if ( !dirFiles[i].getName().matches(salesFilePattern) ) {
				iMismatchFiles++;
				continue;
			}
			// 連番確認
			if ( i - iMismatchFiles > 0 ) {
				Long lBeforeFileNum = Long.valueOf(dirFiles[i - ( iMismatchFiles + 1)].getName().replace(".rcd", ""));
				Long lCurrentFileNum = Long.valueOf(dirFiles[i].getName().replace(".rcd", ""));
				if ( ( lCurrentFileNum - lBeforeFileNum ) != 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return false;
				}
			}
			arrayProceedsList.add(dirFiles[i]);
		}

		for (int i = 0; i < arrayProceedsList.size(); i++) {
			// 支店の売り上げ額を抽出
			BufferedReader brProceeds = null;
			try {
				FileReader frProceeds = new FileReader(arrayProceedsList.get(i));
				brProceeds = new BufferedReader(frProceeds);

				// ArrayListに保持
				ArrayList<String> arrayRcdData = new ArrayList<String>();
				String strLineData = "";
				while ( (strLineData = brProceeds.readLine()) != null ) {
					arrayRcdData.add(strLineData);
				}

				// 2行でなければエラー
				if ( arrayRcdData.size() != 2 ) {
					System.out.println(arrayProceedsList.get(i).getName() +"のフォーマットが不正です");
					return false;
				}

				// 支店コードが正しいフォーマットか確認
				String code = arrayRcdData.get(0);
				String codePattern = "^\\d{3}$";
				if ( !code.matches(codePattern) ) {
					// Error[24]
					System.out.println(arrayProceedsList.get(i).getName() +"の支店コードが不正です");
					return false;
				}

				// 支店コードが定義ファイルに存在するか確認
				if ( !proceedsList.containsKey(code) ) {
					System.out.println(arrayProceedsList.get(i).getName() +"の支店コードが不正です");
					return false;
				}

				// 金額を取得、数字でなければエラー
				String strValue = arrayRcdData.get(1);
				if ( !strValue.matches("^\\d+$") ) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
				Long sales = Long.valueOf(strValue);

				Long currentValue = proceedsList.get(code);
				Long sumValue = (currentValue += sales);
				// 合計金額が10桁を超えた場合エラー
				if (sumValue >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return false;
				}
				proceedsList.replace(code, sumValue);
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			} finally {
				try {
					if ( brProceeds != null ) {
						brProceeds.close();
					}
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	private static boolean writeOutFile (String filePath, String fileName, HashMap<String, String> branchList, HashMap<String, Long> proceedsList) {
		// 支店別集計ファイルを作成し、支店コード、支店名、合計金額を出力
		BufferedWriter bwBranchOut = null;
		File fileBranchOut = new File(filePath, fileName);
		try {
			FileWriter fwBranchOut = new FileWriter(fileBranchOut);
			bwBranchOut = new BufferedWriter(fwBranchOut);
			// 支店定義ファイルの内容と売上ファイルを照合
			for ( HashMap.Entry<String, String> branchEntry :  branchList.entrySet()){
				for ( HashMap.Entry<String, Long> proceedsEntry : proceedsList.entrySet()) {
					if ( branchEntry.getKey().equals(proceedsEntry.getKey()) ) {
						// 支店コード、支店名、合計金額を記述
						bwBranchOut.write(
								branchEntry.getKey() + "," +
								branchEntry.getValue() + "," +
								proceedsEntry.getValue()
								);
						bwBranchOut.newLine();
					}
				}
			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try {
				if ( bwBranchOut != null ) {
					bwBranchOut.close();
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}
}
